//
//  ViewController.h
//  p01-papastrat
//
//  Created by Vasili Papastrat on 1/26/16.
//  Copyright © 2016 Vasili Papastrat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic,strong) IBOutlet UILabel *lbHelloWorld;
@property (strong, nonatomic) IBOutlet UIButton *btClickMe;
@property (strong, nonatomic) IBOutlet UIImageView* ivMF;
@property int state;

-(IBAction)bt_OnClick:(id)sender;

@end

