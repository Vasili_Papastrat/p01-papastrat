//
//  AppDelegate.h
//  p01-papastrat
//
//  Created by Vasili Papastrat on 1/26/16.
//  Copyright © 2016 Vasili Papastrat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

