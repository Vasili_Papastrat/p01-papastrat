//
//  ViewController.m
//  p01-papastrat
//
//  Created by Vasili Papastrat on 1/26/16.
//  Copyright © 2016 Vasili Papastrat. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize lbHelloWorld;
@synthesize btClickMe;
@synthesize ivMF;
@synthesize state;

- (void)viewDidLoad {
    state = 0;
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)bt_OnClick:(id)sender{
    [self changeState];
}

-(void)changeState{
    if(state == 0){
        [lbHelloWorld setText:@"See. It works!"];
        [ivMF setHidden:false];
        [btClickMe setTitle:@"Told you." forState:UIControlStateNormal];
        state = 1;
    }else{
        [lbHelloWorld setText:@"Hello World"];
        [ivMF setHidden:true];
        [btClickMe setTitle:@"Click Me, you won't regret it!" forState:UIControlStateNormal];
        state = 0;
    }
}

@end
