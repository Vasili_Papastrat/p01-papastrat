//
//  main.m
//  p01-papastrat
//
//  Created by Vasili Papastrat on 1/26/16.
//  Copyright © 2016 Vasili Papastrat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
